package com.atlassian.test.rules;

import org.junit.AssumptionViolatedException;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public class InvertResult implements TestRule {

    @Override
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                try {
                    base.evaluate();
                } catch(AssumptionViolatedException e){
                } catch (AssertionError e) {
                    return;
                }
                throw new AssertionError("No assertion failed");

            }
        };
    }
}