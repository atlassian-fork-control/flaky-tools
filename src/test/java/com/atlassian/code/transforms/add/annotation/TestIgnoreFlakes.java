package com.atlassian.code.transforms.add.annotation;

import com.atlassian.test.rules.Flaky;
import com.atlassian.test.rules.IgnoreFlakes;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;

import static org.junit.Assert.fail;

public class TestIgnoreFlakes {

    @Rule
    public RuleChain chain = RuleChain.emptyRuleChain()
            .around(new IgnoreFlakes());

    @Test
    @Flaky(jiraIssue = "FAKE-1234", expiryDate = "9999-12-31")
    public void IgnoredTest() {
        fail("Should have been ignored by Flaky annotation");
    }


}
